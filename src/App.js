import logo from './logo.svg';
import './App.css';
import { Button, Card, Col, Container, Nav, Navbar, Row } from 'react-bootstrap';


function App() {
  return (
    <>
      <Navbar bg="dark" variant="dark" style={{ height: '10vh', }} >
        <Navbar bg="dark" variant="dark">
          <Container>
            <Navbar.Brand href="#home" style={{ fontSize: '24px', color: 'white', fontWeight: 'bold', letterSpacing: '2px', fontFamily: 'revert', marginRight: '15px', paddingRight: '200px' }}>
              <img
                alt=""
                src="https://www.istad.co/resources/img/logo_md.png"
                width="40"
                height="40"
                className="d-inline-block align-top"
              />{' '}
              ISTAD
            </Navbar.Brand>
          </Container>
        </Navbar>
        <Container>
          <Nav className="me-auto">
            <Nav.Link href="#home" style={{ fontSize: '18px', color: 'white', fontWeight: 'bold', letterSpacing: '2px', fontFamily: 'revert', marginRight: '15px' }}>Home</Nav.Link>
            <Nav.Link href="#features" style={{ fontSize: '18px', color: 'white', fontWeight: 'bold', letterSpacing: '2px', fontFamily: 'revert', marginRight: '15px' }}>Enroll</Nav.Link>
            <Nav.Link href="#pricing" style={{ fontSize: '18px', color: 'white', fontWeight: 'bold', letterSpacing: '2px', fontFamily: 'revert', marginRight: '15px' }}>Course</Nav.Link>
            <Nav.Link href="#pricing" style={{ fontSize: '18px', color: 'white', fontWeight: 'bold', letterSpacing: '2px', fontFamily: 'revert', marginRight: '15px' }}>IT News</Nav.Link>
            <Nav.Link href="#pricing" style={{ fontSize: '18px', color: 'white', fontWeight: 'bold', letterSpacing: '2px', fontFamily: 'revert', marginRight: '15px' }}>Job Opportunity</Nav.Link>
            <Nav.Link href="#pricing" style={{ fontSize: '18px', color: 'white', fontWeight: 'bold', letterSpacing: '2px', fontFamily: 'revert', marginRight: '15px' }}>About Us</Nav.Link>

          </Nav>
        </Container>
      </Navbar>
      <div>
        <p style={{fontSize : '35px',fontWeight : 'bolder',fontFamily : 'revert',letterSpacing :'1px',paddingTop : '20px',paddingLeft : '150px',color : '#212529'}}>COURSES</p>
    </div>
     <Container>
      <Row className='g-5'>
        <Col md={4} style={{border : 'none'}}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/354762b5-e27a-40ac-93c3-6de5cdb0362f.png" />
      <Card.Body>
        <Card.Title>SQL & DATA MODELING </Card.Title>
        <Card.Text>
          SQL & Data Modeling with PostgreSQL is designed to help you understand in...
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card >
        </Col>
        <Col md={4}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/835a7298-dbab-448d-8229-ee8a555c4846.png" />
      <Card.Body>
        <Card.Title>DEVOPS ENGINEERING</Card.Title>
        <Card.Text>
          This course will provide you with in-depth knowledge on how to build...
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card>
        </Col>
        <Col md={4}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/c8c41751-3bc0-4f07-9658-7d95efbae692.png" />
      <Card.Body>
        <Card.Title>SPRING FRAMEWORK</Card.Title>
        <Card.Text>
          Spring Course will help in understanding about Spring framework and how to...
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card>
        </Col>
        <Col md={4} style={{border : 'none'}}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/b6887a37-08ba-404c-9dff-3a56ad49def7.png" />
      <Card.Body>
        <Card.Title>FULL STACK WEB DEVELOPMENT </Card.Title>
        <Card.Text>
          A full stack web development course is designed to develop both client and...
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card >
        </Col>
        <Col md={4}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/3d7beaad-309a-4f2f-91e2-b3be705ef3f6.png" />
      <Card.Body>
        <Card.Title>DOCKER</Card.Title>
        <Card.Text>
          Docker is a containerized tool that designed to make it easier to create...
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card>
        </Col>
        <Col md={4}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/b0b3f26d-4d21-45c6-9ed4-8e1f9f11e534.png" />
      <Card.Body>
        <Card.Title>JAVA PROGRAMMING</Card.Title>
        <Card.Text>
          Java is a high-level, class object-oriented programming language that is..
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card>
        </Col>
        <Col md={4} style={{border : 'none'}}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/4709913c-3dd4-4896-8b05-ba6f7fd074ba.png" />
      <Card.Body>
        <Card.Title>IOS DEVELOPMENT </Card.Title>
        <Card.Text>
          IOS Development is designed for students to get started with design IOS
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card >
        </Col>
        <Col md={4}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/3a3d2bf2-670b-4f2f-a72a-b9b8ba3a0f38.png" />
      <Card.Body>
        <Card.Title>WEB DESIGN</Card.Title>
        <Card.Text>
          Web Design course is designed for students to get started with Design...
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card>
        </Col>
        <Col md={4}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/899bac49-e47c-406c-abb2-30ad0b498f88.png" />
      <Card.Body>
        <Card.Title>FLUTTER MOBILE DEVELOPMENT</Card.Title>
        <Card.Text>
          Flutter course is designed to develop multi-platform like ios and Android app..
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card>
        </Col>
        <Col md={4} style={{border : 'none'}}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/0d254e62-5896-4c61-b1aa-c7de02d8d40d.png" />
      <Card.Body>
        <Card.Title>ANDROID DEVELOPMENT </Card.Title>
        <Card.Text>
          Android is an open source and Linux-based operating system for mobile.
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card >
        </Col>
        <Col md={4}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/dea9d967-e99e-49f3-a999-5b710dd5daf3.png" />
      <Card.Body>
        <Card.Title>NEXT.JS</Card.Title>
        <Card.Text>
          Next.js course is designed for students to develop single page application. it h..
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card>
        </Col>
        <Col md={4}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/73c3424a-39e0-450c-8969-8b35559594f5.png" />
      <Card.Body>
        <Card.Title>C++ PROGRAMMING</Card.Title>
        <Card.Text>
          C++ is one of the world's most popular programming languages. C++ can be..
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card>
        </Col>
        <Col md={4} style={{border : 'none'}}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/b9bf96d7-c352-4533-8025-f017517ba648.png" />
      <Card.Body>
        <Card.Title>LINUX ADMINISTRATOR </Card.Title>
        <Card.Text>
          Linux is a family of open-source Unix-like operating systems based on the Linux...
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card >
        </Col>
        <Col md={4}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/24c392c2-152b-49c0-bddb-dc12b9403b3e.png" />
      <Card.Body>
        <Card.Title>BLOCKCHAIN DEVELOPMENT</Card.Title>
        <Card.Text>
          The Blockchain is a technology based on a decentralized network with "blocks"...
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card>
        </Col>
        <Col md={4}>
        <Card style={{border : 'none', backgroundColor : '#212529',color : 'white',borderRadius : '20px'}}>
      <Card.Img variant="top" src="https://api.istad.co/media/image/a3c4f87e-7a85-44c3-a568-6c5abef76cfe.png" />
      <Card.Body>
        <Card.Title>DATA ANALYTICS</Card.Title>
        
        <Card.Text>
          Data analytics is the science of analyzing raw data to make conclusion about the....
        </Card.Text>
        <Button variant="light" style={{color : 'black',fontWeight : 'bolder',fontFamily : 'unset'}}>More Information</Button>{' '}
      </Card.Body>
    </Card>
        </Col>
      </Row>
     </Container>

    </>
  );
}

export default App;
